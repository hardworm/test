{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}
{block name=css}
    <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/plugins/fullcalendar/fullcalendar/fullcalendar.css">
    <link rel="stylesheet" type="{ci helper="url" function="base_url"}text/css" href="assets/plugins/select2/select2.css" />
    <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/plugins/DataTables/media/css/DT_bootstrap.css" />
    <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch.css">
    <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
    <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/plugins/datepicker/css/datepicker.css">
    {* css для модальных окон *}
    <link href="{ci helper="url" function="base_url"}assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
    <link href="{ci helper="url" function="base_url"}assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    {* css для модальных окон *}
{/block}

{* This block is defined in the master.php template *}
{block name=body}
    <!-- start: PANEL CONFIGURATION MODAL FORM -->
    <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Panel Configuration</h4>
                </div>
                <div class="modal-body">
                    Here will be a configuration form
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary">
                        Save changes
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- end: SPANEL CONFIGURATION MODAL FORM -->
    <div class="container">
        <!-- start: PAGE HEADER -->
        <div class="row">
            <div class="col-sm-12">
                <!-- start: PAGE TITLE & BREADCRUMB -->
                {*<ol class="breadcrumb">
                    <li>
                        <i class="clip-screen"></i>  
                        <a href="#">Пользователи</a></li>
                    <li class="active">Редактировать пользователя</li>		
                </ol>*}
                <div class="page-header">
                    <h1>Create User</h1>
                </div>
                <!-- end: PAGE TITLE & BREADCRUMB -->
            </div>
        </div>
        <!-- end: PAGE HEADER -->

        <div class="row">
            <div class="col-sm-12">
                <!-- start: TEXT FIELDS PANEL -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i>
                        Create User
                        <div class="panel-tools">
                            <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                            </a>
                            <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <a class="btn btn-xs btn-link panel-refresh" href="#">
                                <i class="fa fa-refresh"></i>
                            </a>
                            <a class="btn btn-xs btn-link panel-expand" href="#">
                                <i class="fa fa-resize-full"></i>
                            </a>
                            <a class="btn btn-xs btn-link panel-close" href="#">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>


                    <div class="panel-body">
                        <form role="form" class="form-horizontal" action="{ci helper="url" function="base_url"}auth/create_user/{$users.id}" method="POST">

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">UserName</label>
                                <div class="col-sm-9">
                                    <input name="user_name" type="text" required  value="{$user_name}" id="form-field-1" class="form-control">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">FirstName</label>
                                <div class="col-sm-9">
                                    <input name="first_name" required type="text" value="{$first_name}" id="form-field-1" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">LastName</label>
                                <div class="col-sm-9">
                                    <input name="last_name" required type="text" value="{$last_name}" id="form-field-1" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">email</label>
                                <div class="col-sm-9">
                                    <input name="email"  required type="text" value="{$email}" id="form-field-1" class="form-control">
                                </div>
                            </div>
                                
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">password</label>
                                <div class="col-sm-9">
                                    <input name="password"  required type="password" value="{$password}" id="form-field-1" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">password confirm</label>
                                <div class="col-sm-9">
                                    <input name="password_confirm"  required type="password" value="{$password_confirm}" id="form-field-1" class="form-control">
                                </div>
                            </div>
                                
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">Street</label>
                                <div class="col-sm-9">
                                    <input name="Street" type="text" value="{$Street}" id="form-field-1" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">Location</label>
                                <div class="col-sm-9">
                                    <input name="Location" type="text" value="{$Location}" id="form-field-1" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">Zip</label>
                                <div class="col-sm-9">
                                    <input name="Zip" type="text" value="{$Zip}" id="form-field-1" class="form-control">
                                </div>
                            </div>


                            <div class="form-group text-center "><br><br>
                                <button class="btn btn-green" type="submit">Save</button>
                            </div>
                        </form>
                    </div>
                            
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>
        </div>				
    </div>
			
{/block}
{block name=js}
    <script type="text/javascript" src="{ci helper="url" function="base_url"}assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="{ci helper="url" function="base_url"}assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{ci helper="url" function="base_url"}assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/js/table-data.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/autosize/jquery.autosize.min.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/summernote/build/summernote.min.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/ckeditor/adapters/jquery.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/js/form-elements.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"></script>
    {*js для модальных окон *}
    <script src="{ci helper="url" function="base_url"}assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/js/ui-modals.js"></script>
    {*js для модальных окон *}
    
        <script>
            jQuery(document).ready(function() {
				Main.init();
				FormElements.init();
				UIButtons.init();
                UIModals.init();
			});
        </script>		
{/block}