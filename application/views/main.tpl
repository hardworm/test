{* Extend our master template *}
{extends file="index.tpl"}

{* This block is defined in the master.php template *}
{block name=title}
    {$title}
{/block}
{block name=css}
    <link rel="stylesheet" type="text/css" href="{ci helper="url" function="base_url"}assets/plugins/select2/select2.css" />
    <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/plugins/DataTables/media/css/DT_bootstrap.css" />
{/block}

{* This block is defined in the master.php template *}
{block name=body}
    <!-- start: PANEL CONFIGURATION MODAL FORM -->
    <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Panel Configuration</h4>
                </div>
                <div class="modal-body">Here will be a configuration form</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- end: SPANEL CONFIGURATION MODAL FORM -->
    <div class="container">
        <!-- start: PAGE HEADER -->
        <div class="row">
            <div class="col-sm-12">
                <!-- start: PAGE TITLE & BREADCRUMB -->
                <ol class="breadcrumb">
                    <li>
                        <i class="clip-home-3"></i>  
                        <a href="/admin/index.php">Home</a>
                    </li>
                    <li class="active">Dashboard</li>							
                </ol>
                <div class="page-header">
                    <h1>Dashboard <small>overview &amp; stats </small></h1>
                </div>
                <!-- end: PAGE TITLE & BREADCRUMB -->
            </div>
        </div>
        <!-- end: PAGE HEADER -->


        <!-- start: PAGE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- start: DYNAMIC TABLE PANEL -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i>
                        Dynamic Table
                        <div class="panel-tools">
                            <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                            </a>
                            <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <a class="btn btn-xs btn-link panel-refresh" href="#">
                                <i class="fa fa-refresh"></i>
                            </a>
                            <a class="btn btn-xs btn-link panel-expand" href="#">
                                <i class="fa fa-resize-full"></i>
                            </a>
                            <a class="btn btn-xs btn-link panel-close" href="#">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                            <thead>
                                <tr>
                                    <th>Поставщик</th>
                                    <th class="hidden-xs">Время</th>
                                    <th>Дублей в выгрузке</th>
                                    <th class="hidden-xs">Товаров с учетом игнорируемых в выгрузке</th>
                                    <th>Товаров в базе</th>
                                    <th>Есть в БД но нет в выгрузке</th>
                                    <th>Есть в выгрузке но нет в БД</th>
                                    <th>Обновленных записей</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $suppliers AS $val}
                                <tr>
                                    <td>{if $val.supplier_id !=""}<a href="{ci helper="url" function="base_url"}main/view/{$val.supplier_id}">{$val.supplier_name}</a>{else}{$val.supplier_name}{/if}</td>
                                    <td class="hidden-xs">{$val.time}</td>
                                    <td>{$val.count_dubles}</td>
                                    <td class="hidden-xs">{$val.count_products}</td>
                                    <td>{$val.count_products_in_db}</td>
                                    <td>{$val.count_in_db_not_in_xml}</td>
                                    <td>{$val.count_in_xml_not_in_db}</td>
                                    <td>{$val.count_upd}</td>
                                </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- end: DYNAMIC TABLE PANEL -->
            </div>
        </div>

        <!-- end: PAGE CONTENT-->


    </div>
{/block}
{block name=js}
    <script type="text/javascript" src="{ci helper="url" function="base_url"}assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="{ci helper="url" function="base_url"}assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{ci helper="url" function="base_url"}assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
    <script src="{ci helper="url" function="base_url"}assets/js/table-data.js"></script>
    
    <script>
        jQuery(document).ready(function () {
            Main.init();
            TableData.init();
        });
    </script>		
{/block}