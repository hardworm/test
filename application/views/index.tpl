<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.3 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- start: HEAD -->
    <head>
        <title>{block name=title}{/block}</title>
        <!-- start: META -->
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: MAIN CSS -->
        <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/fonts/style.css">
        <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/css/main.css">
        <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/css/main-responsive.css">
        <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/plugins/iCheck/skins/all.css">
        <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
        <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
        <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/css/theme_light.css" type="text/css" id="skin_color">
        <link rel="stylesheet" href="{ci helper="url" function="base_url"}assets/css/print.css" type="text/css" media="print"/>
        <!--[if IE 7]>
        <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
        <![endif]-->
        <!-- end: MAIN CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        {block name=css}{/block}
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body>
        <!-- start: HEADER -->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <!-- start: TOP NAVIGATION CONTAINER -->
            <div class="container">
                <div class="navbar-header" style="">
                    <!-- start: RESPONSIVE MENU TOGGLER -->
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="clip-list-2"></span>
                    </button>
                    <!-- end: RESPONSIVE MENU TOGGLER -->
                    <!-- start: LOGO -->
{*                    <a class="navbar-brand" href="/"><img src="/admin/images/logo.png" alt="logo" height="45" width="320" /></a>*}
                    <!-- end: LOGO -->
                </div>

            </div>
            <!-- end: TOP NAVIGATION CONTAINER -->
        </div>
        <!-- end: HEADER -->
        <!-- start: MAIN CONTAINER -->
        <div class="main-container">
            <div class="navbar-content">
                <!-- start: SIDEBAR -->
                <div class="main-navigation navbar-collapse collapse">
                    <!-- start: MAIN MENU TOGGLER BUTTON -->
                    <div class="navigation-toggler">
                        <i class="clip-chevron-left"></i>
                        <i class="clip-chevron-right"></i>
                    </div>
                    <!-- end: MAIN MENU TOGGLER BUTTON -->
                    <!-- start: MAIN NAVIGATION MENU -->
                    <ul class="main-navigation-menu">
                        <li>
                            <a href="index.php"><i class="clip-home-3"></i>
                                <span class="title"> Dashboard </span><span class="selected"></span>
                            </a>
                        </li>

                        <li>
                            <a href="{ci helper="url" function="base_url"}auth/logout"><i class="clip-exit"></i>
                                <span class="title">Выход</span><span class="selected"></span>
                            </a>
                        </li>

                    </ul>					<!-- end: MAIN NAVIGATION MENU -->
                </div>
                <!-- end: SIDEBAR -->
            </div>
            <!-- start: PAGE -->
            <div class="main-content">
                {block name=body}{/block}
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <div class="footer clearfix">
            <div class="footer-inner">
                &copy; hardworm 
            </div>
            <div class="footer-items">
                <span class="go-top"><i class="clip-chevron-up"></i></span>
            </div>
        </div>
        <!-- end: FOOTER -->
        <div id="event-management" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">Event Management</h4>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                            Close
                        </button>
                        <button type="button" class="btn btn-danger remove-event no-display">
                            <i class='fa fa-trash-o'></i> Delete Event
                        </button>
                        <button type='submit' class='btn btn-success save-event'>
                            <i class='fa fa-check'></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- start: MAIN JAVASCRIPTS -->
        <!--[if lt IE 9]>
        <script src="{ci helper="url" function="base_url"}assets/plugins/respond.min.js"></script>
        <script src="{ci helper="url" function="base_url"}assets/plugins/excanvas.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <![endif]-->
        <!--[if gte IE 9]><!-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <!--<![endif]-->
        <script src="{ci helper="url" function="base_url"}assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
        <script src="{ci helper="url" function="base_url"}assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="{ci helper="url" function="base_url"}assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
        <script src="{ci helper="url" function="base_url"}assets/plugins/blockUI/jquery.blockUI.js"></script>
        <script src="{ci helper="url" function="base_url"}assets/plugins/iCheck/jquery.icheck.min.js"></script>
        <script src="{ci helper="url" function="base_url"}assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
        <script src="{ci helper="url" function="base_url"}assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
        <script src="{ci helper="url" function="base_url"}assets/plugins/less/less-1.5.0.min.js"></script>
        <script src="{ci helper="url" function="base_url"}assets/plugins/jquery-cookie/jquery.cookie.js"></script>
        <script src="{ci helper="url" function="base_url"}assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
        <script src="{ci helper="url" function="base_url"}assets/js/main.js"></script>
        <!-- end: MAIN JAVASCRIPTS -->
       
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        {block name=js}{/block}
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    </body>

    <!-- end: BODY -->
</html>