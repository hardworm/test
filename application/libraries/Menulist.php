<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menulist {
    private $i = 1;
    private $cats;
    private $gid;
    private $pr;
    private $gr;
    private $child;
    private $url;

    function __construct() {
        $this->gid  = 1;
        $this->cats = $this->getCats();
    }

    public function getCats() {
        $levels = array();
        $tree   = array();
        $cur    = array();

        $CI = get_instance();
        $CI->load->database();
        $CI->load->helper('url');
        $CI->load->model('category_model');
        $res = $CI->category_model->getGroups();
        
        $CI->load->config('config');
        $this->url = config_item('base_url');

        foreach ($res as $rows) {
            $cur              = &$levels[$rows['id']];
            $cur['parent_id'] = $rows['parent_id'];
            $cur['name']      = $rows['name'];
            $cur['visible']   = $rows['visible'];
            $this->gr[$rows['id']] = $rows['parent_id'];
            $this->child[$rows['parent_id']][] = $rows['id'];

            if ($rows['parent_id'] == 1) {
                $tree[$rows['id']] = &$cur;
            } else {
                $levels[$rows['parent_id']]['children'][$rows['id']] = &$cur;
            }
        }
        return $tree;
    }

    public function getArrParents($gid) {
       $this->getParents($gid);
       return $this->pr[$this->gid];
    }
    
    private function getParents($gid) {
        $this->pr[$this->gid][] = $this->gr[$gid];
        if($this->gr[$gid] != 1){
            $this->getParents($this->gr[$gid]);
        } 
    }

    public function getMenu($url) {
        return $this->getTree($this->cats,$url);
    }
    
    public function getMenuOptions($gid) {
        return $this->getTreeOptions($this->cats,$gid);
    }

    private function getTree($arr, $url) {
        $this->i++;
        $out = '';

        $out .= "<ul class='all-shop-lvl$this->i'>";
        foreach ($arr as $id => $val) {
            if($id == 1 /*OR ($this->i > 1 AND !in_array($val['parent_id'], $this->getArrParents($this->gid)) AND @!in_array($id, $this->child[$this->gid]))*/){
                continue;
            }
            if($id == $this->gid OR $id == $this->gr[$this->gid]) {
                $out .= " <ul class='all-shop-lvl$this->i'><a href='{$url}$id'>$val[name] [$val[visible]]</a></ul> ";
            } else {
                $out .= " <li class='all-shop-lvl$this->i'><a href='{$url}$id'>$val[name] [$val[visible]]</a> ";
                if (!empty($val['children'])) {
                    $out .= $this->getTree($val['children'], $url);
                }
                $out .= " </li> ";
            }
            
            
        }
        $out .= "</ul>";
        $this->i--;
        return $out;
    }
    private function getTreeOptions($arr, $gid) {
        $this->i++;
        $out = '';

        foreach ($arr as $id => $val) {
//            if($id == 1 ){
//                continue;
//            }
            $class = ($val['visible'] == 1) ? "class='disabled'" : '';
            $visible = ($val['visible'] == 0) ? " (скрытая категория)" : '';
            
            $nbsp = "";
            for ($index = 1; $index < $this->i; $index++) {
                $nbsp .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            }
            
            if($id == @$this->gr[@$gid]) {
                $out .= " <option $class value='$id' SELECTED >{$nbsp}{$val['name']}$visible</option> ";
            } else {
                $out .= " <option $class value='$id'>{$nbsp}{$val['name']}$visible</option> ";
            }
            
            if (!empty($val['children'])) {
                $out .= $this->getTreeOptions($val['children'],$id);
            }
        }
        $this->i--;
        return $out;
    }
}