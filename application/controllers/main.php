<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main extends My_Controller {

    public function __construct() {
        parent::__construct();

        // Ideally you would autoload the parser
        $this->load->database();
        $this->load->helper(array('url', 'language'));
        $this->load->library(array('parser', 'ion_auth', 'form_validation'));
    }

    public function index() {
        // Some example data
        $data['title'] = "Панель администрирования";
        

        // Load the template from the views directory
        redirect('auth', 'refresh');
    }
    
}