/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
 CKEDITOR.replace( 'editor1',
{
	filebrowserBrowseUrl : '/admin/assets/plugins/ckeditor/plugins/ckfinder.html',
	filebrowserImageBrowseUrl : '/admin/assets/plugins/ckeditor/plugins/ckfinder.html?type=Images',
	filebrowserFlashBrowseUrl : '/admin/assets/plugins/ckeditor/plugins/ckfinder.html?type=Flash',
	filebrowserUploadUrl : '/admin/assets/plugins/ckeditor/plugins/core/connector/php/connector.php?command=QuickUpload&type=Files',
	filebrowserImageUploadUrl : '/admin/assets/plugins/ckeditor/plugins/core/connector/php/connector.php?command=QuickUpload&type=Images',
	filebrowserFlashUploadUrl : '/admin/assets/plugins/ckeditor/plugins/core/connector/php/connector.php?command=QuickUpload&type=Flash'
});};
